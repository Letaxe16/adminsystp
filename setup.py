#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 12:16:42 2022

@author: axel.guillet
"""

#from setuptools import setup
#
#with open("README.md", "r", encoding="utf-8") as fh:
#    long_description = fh.read()
#    
#setup{
#      name='TestNumberSuite',
#      version='0.0.1',
#      author="Axel Guillet",
#      author_email="axel.guillet6@gmail.com",
##      packages=['calculator'],
#      description="TestSimpleCalculator is a simple package \
#      in order to make some test on packaging principle with basic calcul"
#      long_description=description,
#      long_description_content_type="text/markdown",
#      url="https://gitlab.com/Letaxe16/adminsystp",
#      project_urls={
#        "Bug Tracker": "https://gitlab.com/Letaxe16/adminsystp/calculator",
#       },
#       classifiers=[
#            "Programming Language :: Python :: 3",
#            "License :: OSI Approved :: MIT License",
#            "Operating System :: OS Independent",
#        ],
#        package_dir={"": "src"},
#        packages=setuptools.find_packages(where="src"),
#        python_requires=">=3.6",
#      }


# Finir les modifications de ce code avant d econtinuer l'exo 9


from setuptools import setup 
import setuptools 

setup(     
      name='TestNumberSuite',     
      version='0.0.1',     
      author="Axel Guillet",     
      description="TestSimpleCalculator is a simple package \
      in order to make some test on packaging principle with basic calcul",   
      license='GNU GPLv3',     
      python_requires ='>=3.4',     
      package_dir={"": "calculator"},     
      packages=setuptools.find_namespace_packages(where="calculator"), 
)

