#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 13:45:10 2022

@author: axel.guillet
"""
import logging


logging.basicConfig(filename='log_error', level=logging.ERROR)

class SimpleCalculator:
    """
    Classe réalisant les opérations traditionnelles
    """

    def isInstance(self,a_int):
        """
        Fonction permettant de vérifier si le nombre entré est un entier
        Entrée: 1 entier
        Sortie: 1 booléen
        """
        it_is = False
        if (a_int == int(a_int)):
            it_is = True
        return it_is


    def sum(self,a_int1,b_int1):
        """
        Fonction réalisant une addition
        Entrée: 2 entiers
        Sortie: 1 entier
        """
        res = 0
        if self.isInstance(a_int1) and self.isInstance(b_int1):
            res = a_int1 + b_int1
        else:
            logging.error("Carefull! You didn't input an integer variable")
            return "ERROR"
        logging.info("Tout s'est bien passé comme prévu!")
        return res


    def substract(self,a_int2,b_int2):
        """
        Fonction réalisant une soustraction
        Entrée: 2 entiers
        Sortie: 1 entier
        """
        res = 0
        if self.isInstance(a_int2) and self.isInstance(b_int2):
            res = a_int2 - b_int2
        else:
            logging.error("Carefull! You didn't input an integer variable")
            return "ERROR"
        logging.info("Tout s'est bien passé comme prévu!")
        return res


    def multiply(self,a_int3,b_int3):
        """
        Fonction réalisant une multiplication
        Entrée: 2 entiers
        Sortie: 1 entier
        """
        res = 0
        if self.isInstance(a_int3) and self.isInstance(b_int3):
            res = a_int3 * b_int3
        else:
            logging.error("Carefull! You didn't input an integer variable")
            return "ERROR"
        logging.info("Tout s'est bien passé comme prévu!")
        return res


    def divide(self,a_int4,b_int4):
        """
        Fonction réalisant une multiplication
        Entrée: 2 entiers
        Sortie: 1 entier
        """
        res = 0
        if self.isInstance(a_int4) and self.isInstance(b_int4):
            if b_int4 != 0:
                res = a_int4 / b_int4
            elif b_int4 == 0:
                logging.warning('Watch out! You divided by zero')
                raise ZeroDivisionError("Cannot divide by zero") 
        else:
            logging.error("Carefull! You didn't input an integer variable")
            return "ERROR"
        logging.info("Tout s'est bien passé comme prévu!")
        return res
    
    """
                try:
                return self.__a/self.__b
            except ZeroDivisionError:
                return "Division par 0"
                """
