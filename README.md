# RenduFinal_Calculator_GUILLET

## I. Objectif

L'objectif de ce projet est d'implémenter un calculateur réalisant les 4 opérations de bases, avec également une classe de tests fonctionnels vérifiant le bon fonctionnement de la classe de calcul. La gestion de package, ainsi qu'une bonne distribution du projet ont aussi été des objectifs majeurs.

## II. Organisation du projet

Voici l'arborescence du projet :


```bash
.
├── calculator
│   ├── __init__.py
│   ├── Package_calculator
│   │   ├── SimpleCalculator.py
│   │   ├── __init__.py
│   └── TestNumberSuite.egg-info
├── test
│   ├── Main.py
│   ├── __init__.py
│
├── dist
│   ├── TestNumberSuite-0.0.1.tar.gz
│   ├── TestNumberSuite-0.0.1-py3-none-any.whl
│
├── log_error
├── pyproject.toml
├── README.md
└── setup.py
```

Le projet est donc composé d'un fichier commentant le fonctionnement du projet (README.md), de fichiers permettant la distribution du projet sur TestPypi, ainsi qie l'lintégration continue sur Gitlab (setup.py et requirements.txt), avec le contenu du dossier dist, ainsi que deux packages:

Le premier est le package calculator contenait la classe implémentant les différents calculs. Le deuxième permet de réaliser les tests.


### Package_calculator

C'est dans ce package qu'est définie le script SimpleCalculator.py, contenant la classe SimpleCalculator, qui implémente des méthodes correspondant aux opérations élémentaires (sum, substract, multiply, divide). Ce package apparaît donc comme une librairie qu'il faudra importer afin d'utiliser les méthodes pour calculer.

Une fonction isInstance a aussi été ajoutée au projet, elle permet de vérifier que le paramètre qui lui a été donné est un entier.

Par exemple voici la méthode divide:

```python
    def divide(self,a_int4,b_int4):
        """
        Fonction réalisant une multiplication
        Entrée: 2 entiers
        Sortie: 1 entier
        """
        res = 0
        if self.isInstance(a_int4) and self.isInstance(b_int4):
            if b_int4 != 0:
                res = a_int4 / b_int4
            elif b_int4 == 0:
                logging.warning('Watch out! You divided by zero')
                raise ZeroDivisionError("Cannot divide by zero") 
        else:
            logging.error("Carefull! You didn't input an integer variable")
            return "ERROR"
        logging.info("Tout s'est bien passé comme prévu!")
        return res

```
On peut remarquer qu'une gestion d'erreur a été réalisée, notamment en ce qui concerne l'interdiction d'une division par 0.



### test

C'est dans ce Package que sont gérés tous les tests sur les principales méthodes du package calculator, permettant ainsi de tester si les méthodes fonctionnent toujours correctement malgré les modifications qu'on pourrait apporter au package calculator. Les tests sont gérés dans le script Calculator_test.py, qui définit une classe de tests unitaires, avec 5 méthodes de tests vérifiant le bon fonctionnement des fonctions de calculs élémentaires (contrôle de la réception de l'exception lors d'une division par 0). Ces méthodes s'exécutent instantanément et automatiquement dès le lancemement du script de Calculator_test.py, grâce au module unittest.

Par exemple voici l'exemple de 2 méthodes testant le calcul de la division (division par un entier négatif + division par 0):

```python
    def test_division_negative_integers(self):
        result = self.calculator.divide(-30,-6)
        self.assertEqual(result,5)
        self.assertNotEqual(result,-5)
    
    
    def test_divide_by_zero_exception(self):
        with self.assertRaises(ZeroDivisionError):
            self.calculator.divide(10,0)

```
## III. Importation des packages

Avant de pouvoir utiliser les packages ensemble, il faut importer les packages et leur contenu, pour cela il existe trois méthodes différentes :

### 1. PYTHONPATH

La première solution, relativement contraignante mais rapide à mettre en place est de placer le PYTHONPATH à la racine du projet, pour cela il faut ouvrir un terminal et se placer à la racine du projet et entrez la commande suivante :

```bash
export PYTHONPATH=$PYTHONPATH:"." 
```

```bash
env | grep PYTHON
```

Cette commande nous permet de tester que le PYTHONPATH ait bien été initialisé.

```python
from calculator.Package_calculator.SimpleCalculator import SimpleCalculator
```

Il faut ensuite rajouter l'import de la classe SimpleCalculator au début de notre script de test Calculator_test.py, pour que le script puisse accéder à la classe SimpleCalculator et ainsi effectuer ses tests.

On se place ensuite dans le répertoire courant du projet avec notre terminal pour exécuter le programme (à condition de bien renseigner le chemin vers ce programme).

```bash
python3 test/Calculator_test.py
```

### 2. SETUP.PY

La deuxième solution est de créer un script python setup.py qui va permettre d'importer un package, pour cela il faut dans ce script indique le chemin d'accès à ce Package. Il faudra aussi dans certains cas définir un fichier texte requirements permettant de spécifier certaines versions de modules nécessaires.

```bash
python setup.py sdist

pytest
```

si un message d'erreur s'affiche et que le test ne s'effectue pas, alors, il faut créer un environnement virtuel et installer le fichier requirements pour pouvoir éxecuter les tests:

```bash
sudo apt-get install python3-venv

python3 -m venv venv #création de l'environnement virtuel

source ./venv/bin/activate #activation de l'environnement virtuel

pip install -r requirements.txt

python setup.py sdist

pytest
```

Pour sortir du virtual env, il suffit de rentrer la commande : deactivate.

### 3. Test Pypi

La dernière méthode pour installer et importer un package est de passer par la plateforme TestPypi, pour cela il faut créer un compte, créer comme précédemment un fichier setup.py et importer via internet le package souhaiter. Une fois le token créé sur le site de Pypi, voici les différentes commandes à entrer :


```bash
python3 -m pip install --user --upgrade setuptools wheel
python3 setup.py sdist bdist_wheel

python3 -m pip install --user --upgrade twine
python3 -m twine upload --repository testpypi dist/*

sudo apt-get install python3-venv
python3 -m venv venv        #création d'un environnement virtuel (venv)
source ./venv/bin/activate


pip install -i https://test.pypi.org/simple/(nom du projet via notre Token)   #(prendre la commande donnée sur le site test.pytp)
```

Il faudra saisir un username et un mot de passe, le login est __token__ et le mot de passe correspond à la clé du token précédemment créé qu'il suffit de copier.

## IV.  Execution des Tests

Une fois que les packages sont bien importés et installés, on peut effectuer les tests, en exécutant le script Calculator_test.py.

### Calculator_test.py

On peut appeler directement le script, il faut cependant pour cela avoir réglé le PYTHONPATH. Dans un terminal se placer à la racine du projet et entrer la commande :

```bash
python3 test/Calculator_test.py
```

On obtient alors :

![plot](./Images/executionTestTP1.png)

Après, avoir modifié puis vérifié le PYTHONPATH, on peut voir que 5 tests sont effectués avec un commentaire à la fin permettant de voir que tous les tests ont été réalisés avec succès.

## V. Ressources


énoncé TP : https://prod.e-campus.cpe.fr/mod/resource/view.php?id=20215

github de Fabrice Jumel : https://github.com/fabricejumel

sites internet :
https://packaging.python.org/tutorials/packaging-projects/#packaging-your-project

https://python-guide-pt-br.readthedocs.io/fr/latest/dev/virtualenvs.html

https://python.doctor/page-python-modules-package-module-cours-debutants-informatique-programmation

Intégration continue: https://gitlab.com/js-ci-training/ci-hero-unitarytest-python

TestPypi: https://packaging.python.org/tutorials/packaging-projects/#packaging-your-project








