#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 13:45:10 2022

@author: axel.guillet

import sys
sys.path.insert(1,"/fs03/share/users/axel.guillet/home/Documents/4_ETI/Admin_sys/Co/TP1/calculator")

import SimpleCalculator

OU

export PYTHONPATH=$PYTHONPATH:"." dans le répertoire courant
env | grep PYTHON               permet de tester le PYTHONPATH

On se place ensuite dans le répertoire courant dans notre terminal pour 
exécuter le programme:

python3 test/Calculator_test.py

puis on fait la méthode suivante:
"""

from calculator.Package_calculator.SimpleCalculator import SimpleCalculator

import unittest


class TestNumberSuite(unittest.TestCase):

    def setUp(self):
        """Executed before every test case"""
        self.calculator = SimpleCalculator()  # Appel de la classe située dans SimpleCalculator
        
        
    def test_sum(self):
        result = self.calculator.sum(-12,5)
        self.assertEqual(result,-7)
        self.assertNotEqual(result,7)
        
    
    def test_substract(self):
        result = self.calculator.substract(80,5)
        self.assertEqual(result,75)
        
        
    def test_multiply(self):
        result = self.calculator.multiply(5,14)
        self.assertEqual(result,70)
        
        
    def test_division_negative_integers(self):
        result = self.calculator.divide(-30,-6)
        self.assertEqual(result,5)
        self.assertNotEqual(result,-5)
    
    
    def test_divide_by_zero_exception(self):
        with self.assertRaises(ZeroDivisionError):
            self.calculator.divide(10,0)


if __name__ == '__main__':
    unittest.main()
    
    

#    print("La somme est egale a:", calcul.sum())
#    print("La multiplication est egale a:", calcul.multiply())
#    print("La soustraction est egale a:", calcul.substract())
#    print("La division est egale a:", calcul.divide())
